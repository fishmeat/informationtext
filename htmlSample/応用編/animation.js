
// グローバル変数として宣言
var js_canvas;
var context;

var v0_x = 5;
var v0_y = -30;

var ball_x = 0;
var ball_y = 500;

// ページが読み込まれた時に実行する
window.onload = function(){
	js_canvas = document.getElementById('canvasSample');
	context =  js_canvas.getContext('2d');
}


// プロット
function drawBall(x, y){
	context.beginPath();
	context.fillStyle = 'rgb(50, 50, 255)';
	context.arc(x, y, 10, 0, Math.PI*2, false);	
	context.fill();
	context.closePath();
}

var t = 0;
var isRunning = false;
var a = 1;
// 描画
function draw(){
	t = 0;
	animationLoop();
}

var _animationID;
function animationLoop(){
	render();
	_animationID = requestAnimationFrame(animationLoop, 1000/60);
}

function render(){
	canvasClear();

	drawBall(ball_x, ball_y);
	
	ball_x = v0_x * t;
	ball_y = v0_y * t + 1/2 * a * t* t + 500;	
	if( ball_y <= 600){
		t++;
	}else{
		cancelAnimationFrame(_animationID);
		delete rander();
	}
}
// キャンバスのクリア
function canvasClear(){
	context.clearRect(0, 0, js_canvas.width, js_canvas.height);
}

