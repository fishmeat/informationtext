
// グローバル変数として宣言
var js_canvas;
var context;

// ページが読み込まれた時に実行する
window.onload = function(){
	js_canvas = document.getElementById('canvasSample');
	context =  js_canvas.getContext('2d');
}

// グラフの軸
function drawAxis(){
	context.beginPath();
	context.strokeStyle = 'rgb(0,0,0)';
	context.moveTo( 0, js_canvas.height/2);     // 始点
	context.lineTo( js_canvas.width, js_canvas.height/2 );    // 終点
	context.stroke();
	
	context.beginPath();
	context.moveTo( js_canvas.width/2, 0 );     // 始点
	context.lineTo( js_canvas.width/2, js_canvas.height );    // 終点
	context.stroke();
}

// プロット
function plotPoint(x, y){
	context.fillRect( x + js_canvas.width / 2 , -y + js_canvas.height / 2 , 1, 1 );
}

// 描画
function draw(){
	drawAxis();
	for(x= - (js_canvas.width/2) ; x < (js_canvas.width / 2); x++ ){
		plotPoint(x, x * x / 100);	// y = x^2 / 100
		plotPoint(x, x);			// y = x
	}
}

// キャンバスのクリア
function canvasClear(){
	context.clearRect(0, 0, js_canvas.width, js_canvas.height);
}

